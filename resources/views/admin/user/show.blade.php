@extends('admin.layouts.layout')
@section('content')
<div class="form-group required">
    <h2>Show User</h2>
</div>

<div class="row">
    <div class="form-group col-12 required">
        <label>NAME:</label>
        <input type="text" name="name" class="form-control" placeholder="Name" value="{{ $user->name }}" disabled>
    </div>

    <div class="form-group col-12 required">
        <label>EMAIL:</label>
        <input type="text" name="email" class="form-control" placeholder="Email" value="{{ $user->email }}" disabled>
    </div>

    <div class="form-group col-12 required">
        <label>CREATED:</label>
        <input type="text" name="create" class="form-control" placeholder="Create" value="{{ $user->created_at }}" disabled>
    </div>

    <div class="form-group col-12 required">
        <label>UPDATE:</label>
        <input type="text" name="create" class="form-control" placeholder="Update" value="{{ $user->updated_at }}" disabled>

    </div>
</div>
<div class="well well-sm clearfix">
    <a class="btn btn-info" href="{{ route('admin.user.index') }}">Back</a>
</div>

@endsection