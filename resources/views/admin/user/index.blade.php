@extends('admin.layouts.layout')
@section('content')
@php /** @var \Illuminate\Support\ViewErrorBag $errors */ @endphp

<div class="form-group required">
    <h2>Пользователи</h2>
</div>
<div class="pull-left mt-3">
    <a class="btn btn-success" href="{{ route('admin.user.create') }}">Добавить</a>
</div>
@if($errors->any())
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">x</span>
            </button>
            {{ $errors->first() }}
        </div>
    </div>
</div>
@endif
@if(session('success'))
<div class="row justify-content-center">
    <div class="col-md-11">
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            {{ session()->get('success') }}
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-striped text-center">
                <thead>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    {{-- <th>PASSWORD</th> --}}
                    <th>ACTION</th>
                    <th class="th-lg">OPTION</th>
                </thead>
                <tbody>
                    @foreach($paginator as $user)
                    @php /** @var \App\Models\User $user */ @endphp
                    <tr>
                        <form action="{{ route('admin.user.destroy', $user->id)}}" method="POST">
                            <td>{{$user->id }}</td>
                            <td>{{$user->name }}</td>
                            <td>{{$user->email }}</td>
                            {{-- <td>{{$user->password }}</td> --}}
                            <td>{{$user->email_verified_at }}</td>
                            <td>
                                <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-primary">Редактировать</a>
                                @csrf @method('DELETE')
                                <button class="btn btn-danger" type="submit">Удалить</button>
                            </td>

                        </form>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @if($paginator->total()>$paginator->count())
        <br>
        <div class="pagination justify-content-center">
            {{ $paginator->links() }}
        </div>
        @endif
    </div>
</div>
@endsection