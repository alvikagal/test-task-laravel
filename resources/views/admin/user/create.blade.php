@extends('admin.layouts.layout')
@section('content')

<div class="form-group required">
	<h2>Create User</h2>
</div>
@if($errors->any())

<div class="col-md-6">
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-label="close">
			<span aria-hidden="true">x</span>
		</button>
		{{ $errors->first() }}
	</div>
</div>

@endif
<div class="row">
	<div class="col-md-6">
		<form action="{{ route('admin.user.store') }}" method="POST">
			@csrf
			<div class="form-group required">
				<label>NAME</label>
				<input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="name" required>
			</div>

			<div class="form-group required">
				<label>EMAIL</label>
				<input email="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="email" required>
			</div>

			<div class="form-group required">
				<label>PASSWORD</label>
				<input type="password" name="password" value="" class="form-control" placeholder="password" required>
			</div>

			<div class="well well-sm clearfix">
				<a class="btn btn-info" href="{{ route('admin.user.index') }}">Back</a>
				<button class="btn btn-success pull-right" title="Save" type="submit">Create</button>
			</div>
		</form>
	</div>
</div>
{{-- {!! Form::close() !!} --}}
@endsection