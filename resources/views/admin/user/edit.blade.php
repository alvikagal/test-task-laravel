@extends('admin.layouts.layout')
@section('content')
@php /** @var \App\Models\User $user */ @endphp

<div class="form-group required">
	<h2>Edit User</h2>
</div>
@php /** @var \Illuminate\Support\ViewErrorBag $errors */ @endphp
@if($errors->any())
<div class="col-md-6">
	<div class="text-center alert-danger">
		{{ $errors->first() }}
	</div>
</div>
@endif
@if(session('success'))

@endif
<div class="row">
	<div class="col-md-6">
		<form action="{{ route('admin.user.update',$user->id)}}" method="POST">
			@method('PATCH')
			@csrf

			<div class="form-group required">
				<label>NAME</label>
				<input type="text" name="name" value="{{ old('name', $user->name) }}" class="form-control" required>
			</div>

			<div class="form-group required">
				<label>EMAIL</label>
				<input type="text" name="email" value="{{ old('email', $user->email) }}" class="form-control" required>
			</div>

			<div class="form-group required">
				<label>PASSWORD</label>
				<input type="password" name="password" value="{{ $user->password }}" class="form-control" required>
			</div>

			<div class="well well-sm clearfix">
				<a class="btn btn-info" href="{{ route('admin.user.index') }}">Back</a>
				<button class="btn btn-success pull-right" title="Save" type="submit">Update</button>
			</div>
		</form>
	</div>
</div>
@endsection